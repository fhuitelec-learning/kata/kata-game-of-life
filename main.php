<?php

require_once (__DIR__ . '/vendor/autoload.php');

use Evaneos\Kata\GameOfLife\GameOfLifeInitializer;

$board = (new GameOfLifeInitializer(10))->getBoard();

foreach ($board as $x) {
    foreach ($x as $y) {
        $toPrint = ($y === 1) ? 'X' : 'O';

        echo $toPrint . ' ';
    }
    echo "\n";
}