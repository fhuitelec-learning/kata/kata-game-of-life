<?php
namespace Evaneos\Kata\GameOfLife;

class GameOfLifeInitializer
{
    const ALIVE = 1;
    const DEAD = 0;

    /** @var int */
    private $boardSize;
    /** @var array */
    private $board;

    public function __construct($boardSize)
    {
        $this->boardSize = $boardSize;
        $this->minAliveCells = 3;

        $this->init();
    }

    public function init()
    {
        $this->board = [];

        $this->initBoard();
        $this->placeRandomAliveCells();
    }

    private function initBoard()
    {
        for ($cellsNbX = 0; $cellsNbX < $this->boardSize; $cellsNbX++) {
            $this->board[] = [];

            for ($cellsNbY = 0; $cellsNbY < $this->boardSize; $cellsNbY++) {
                $this->board[$cellsNbX][] = self::DEAD;
            }
        }
    }

    private function placeRandomAliveCells()
    {
        for ($aliveCellsNb = 0; $aliveCellsNb < $this->minAliveCells; $aliveCellsNb++) {
            $x = rand(0, $this->boardSize - 1);
            $y = rand(0, $this->boardSize - 1);

            while ($this->board[$x][$y] === self::ALIVE) {
                $x = rand(0, $this->boardSize - 1);
                $y = rand(0, $this->boardSize - 1);
            }

            $this->board[$x][$y] = self::ALIVE;
        }
    }

    /** @return array */
    public function getBoard() { return $this->board; }
}